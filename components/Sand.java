package components;

import concrete.ConcreteBuilder;

public class Sand extends Resource {

    public Sand(final int pMass) {
        super(pMass);
    }

    @Override
    public void release() {
        ConcreteBuilder.addSandMass(mMass);
    }
}
