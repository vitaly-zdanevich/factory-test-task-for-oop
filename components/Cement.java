package components;

import concrete.ConcreteBuilder;

public class Cement extends Resource {

    public Cement(final int pMass) {
        super(pMass);
    }

    @Override
    public void release() {
        ConcreteBuilder.addCementMass(mMass);
    }
}
