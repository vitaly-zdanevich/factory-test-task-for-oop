package components;

import concrete.ConcreteBuilder;

public class Water extends Resource {

    public Water(final int pMass) {
        super(pMass);
    }

    @Override
    public void release() {
        ConcreteBuilder.addWaterMass(mMass);
    }
}
