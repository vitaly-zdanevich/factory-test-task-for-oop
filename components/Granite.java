package components;

import concrete.ConcreteBuilder;

public class Granite extends Resource {

    public Granite(final int pMass) {
        super(pMass);
    }

    @Override
    public void release() {
        ConcreteBuilder.addGraniteMass(mMass);
    }
}
