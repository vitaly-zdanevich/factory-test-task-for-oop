package components;

public class Resource {

    int mMass = 0;

    /** Sand, Granite, Cement or Water */
    public Resource(final int pMass) {
        mMass = pMass;
    }

    public void setMass(final int pMass){
        mMass = pMass;
    }


    public void release() { }
}
