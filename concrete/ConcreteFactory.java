package concrete;

import auto.*;
import components.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Есть завод по изготовлению бетона. Бетон состоит из компонентов: песок, гранит, цемент и вода.
 * Добавляются компоненты в пропорции 50% песок, 25% гранит, 10% цемент, 15% вода.
 * Если компоненты будут добавлены в разном порядке или в разной пропроции -
 * тех условия будут нарушены и блок по созданию беттонной смеси придет в негодность.
 * Все компоненты доставляются автомобилями разной грузоподъемности и формы кузова,
 * и в некоторых случаях песок и гранит может быть доставлен одним и тем же автомобилем.
 *
 * Готовая бетонная смесь доставляется отдельным автомобилем-миксером в течение 24 часов.
 * В день может быть произведено 4 автомобиля-миксера.
 * Нужно сделать заказ на 5 машин бетонной смеси, и получить заказ в течение 24 часов.
 */
public class ConcreteFactory {

    private ConcreteBuilder concreteBuilder;
    private List<Mixer> mAutosMixers = createEmptyMixersAutos();

    /**
     * At the start of the world we initialize new empty mixer-autos.
     * @return list with empty mixer autos.
     */
    private List<Mixer> createEmptyMixersAutos() {
        List<Mixer> autosMixers = new ArrayList<>();
        autosMixers.add(new Mixer());
        autosMixers.add(new Mixer());
        autosMixers.add(new Mixer());
        autosMixers.add(new Mixer());
        autosMixers.add(new Mixer());
        return autosMixers;
    }

    ConcreteFactory() { }

    public static void main(String[] args) {
        ConcreteFactory concreteFactory = new ConcreteFactory();
        List<AutoResourceBase> mAutosRes = concreteFactory.initializeAutosWithResources();

        concreteFactory.concreteBuilder = new ConcreteBuilder(mAutosRes);
        concreteFactory.buffer(); // at the start of the new working day - buffer one mixer
        concreteFactory.makeOrder(5);
    }

    /** At the start of world we initialize list of autos with resources */
    private List<AutoResourceBase> initializeAutosWithResources() {
        List<AutoResourceBase> autos = new ArrayList<>();
        int capacityBig = 0;

        autos = initializeBigAutoWithSand(autos, 4);

        AutoResourceBase auto = new AutoResourceDouble();
        int capacityDoubleSand = auto.getCapacity()[0];
        int capacityDoubleGranite = auto.getCapacity()[1];
        auto.setResource(new Resource[]{new Sand(capacityDoubleSand), new Granite(capacityDoubleGranite)});
        auto.fillResourceDouble();
        autos.add(auto);

        auto = new AutoResourceDouble();
        auto.setResource(new Resource[]{new Sand(capacityDoubleSand), new Granite(capacityDoubleGranite)});
        auto.fillResourceDouble();
        autos.add(auto);

        auto = new AutoResourceBig();
        capacityBig = auto.getCapacity()[0];
        auto.setResource(new Granite[]{new Granite(capacityBig)});
        autos.add(auto);

        auto = new AutoResourceSmall();
        auto.setResource(new Granite[]{new Granite(auto.getCapacity()[0])});
        autos.add(auto);

        auto = new AutoResourceBig();
        auto.setResource(new Cement[]{new Cement(capacityBig)});
        autos.add(auto);

        auto = new AutoResourceBig();
        auto.setResource(new Water[]{new Water(capacityBig)});
        autos.add(auto);

        auto = new AutoResourceSmall();
        auto.setResource(new Water[]{new Water(auto.getCapacity()[0])});
        autos.add(auto);

        return autos;
    }

    /**
     * Sand is our most popular resource (50%) and we often need initialize autos with sand.
     * @param pHowMuchAutosBigCreate How much big autos with sand will be created, filled by sand and added at our list
     */
    private List<AutoResourceBase> initializeBigAutoWithSand(List<AutoResourceBase> pAutos, final int pHowMuchAutosBigCreate) {
        for (int i = 0; i < pHowMuchAutosBigCreate; i++) {
            AutoResourceBase auto = new AutoResourceBig();
            int capacityBig = auto.getCapacity()[0];
            auto.setResource(new Sand[]{new Sand(capacityBig)});
            pAutos.add(auto);
        }
        return pAutos;
    }

    /**
     * Out little factory always have clients and because at day we can prepare only 4 autos - we need to have
     * one buffer auto. This method check - have we one buffer (filled) auto - if no - fill one auto.
     */
    private void buffer() {
        Mixer mixer = mAutosMixers.get(0);
        int mixerCapacity = mixer.getCapacity();
        if (mixer.getConcrete() == 0 && concreteBuilder.getConcreteMass() >= mixerCapacity) {
            fill(mixer);
        }
    }

    /**
     * Fill the mixer auto with finished product - concrete.
     * @param pMixer - empty mixer-auto that will be filled by concrete
     */
    private void fill(Mixer pMixer) {
        concreteBuilder.minusFromTotalConcrete(pMixer.getCapacity());
        pMixer.setConcrete(pMixer.getCapacity());
    }

    /**
     * Here we fill mixer-autos with created concrete and start autos to the long way to client.
     * In our simulation program live one day - and at this day we make one order,
     * and this order have limit for max number of filler mixer-autos (maxPossibleFillings).
     * @param pMixersOrder
     */
    private void makeOrder(final int pMixersOrder){
        int maxPossibleFillings = 4;
        for (int i = 0; i < pMixersOrder; i++, maxPossibleFillings--){
            Mixer mixer = mAutosMixers.get(i);
            int mixerCapacity = mixer.getCapacity();
            if (mixer.getConcrete() == 0 && concreteBuilder.getConcreteMass() >= mixerCapacity && maxPossibleFillings >= 0) {
                fill(mixer);
            }
               mixer.go();
        }
    }
}