package concrete;

import auto.AutoResourceBase;

import java.util.List;

/**
 * Here we actively using static fields and methods for simplicity - and by logic we have only one Builder.
 */
public class ConcreteBuilder {

    private static double mSandMass;
    private static double mGraniteMass;
    private static double mCementMass;
    private static double mWaterMass;

    private static int mConcreteMass;

    public ConcreteBuilder() {
        super();
    }

    /** Here we release resources from all autos and start process of constructing concrete.
     * This is some of example of polymorphism - virtual method invocation: auto.releaseResource();
     * */
    public ConcreteBuilder(List<AutoResourceBase> pAutos) {
        for(AutoResourceBase auto : pAutos){
            auto.releaseResource();
        }
        constructConcrete();
    }

    /**
     * Here we construct concrete. We calculate one percent of mass of all resources and after that
     * in cycle taking as much resources as we need for minimal resource - cement
     * (10% overall - here minimum cycle is 1% of cement).
     * We do this while we have all mass of resources that we need for taking next percent of resources -
     * 1% for cement and other resources as much as we need for 1% of cement:
     *
     * 1% cement
     * 5% sand
     * 2,5% granite
     * 1,5% water
     */
    private void constructConcrete() {
        double totalMas = mSandMass + mGraniteMass + mCementMass + mWaterMass;
        double percentMas = totalMas / 100;
        double takenSand = 0;
        double takenGranite = 0;
        double takenCement = 0;
        double takenWater = 0;
        while(mCementMass >=  percentMas  &&
                mSandMass >= (percentMas * 5.0) &&
             mGraniteMass >= (percentMas * 2.5) &&
               mWaterMass >= (percentMas * 1.5) ) {

            takenCement += percentMas;
            mCementMass -= percentMas;

            takenSand += percentMas * 5;
            mSandMass -= percentMas * 5;

            takenGranite += percentMas * 2.5;
            mGraniteMass -= percentMas * 2.5;

            takenWater += percentMas * 1.5;
            mWaterMass -= percentMas * 1.5;
        }

        ConcreteBuilder concreteBuilder = new ConcreteBuilder();

        // you can't mix setters - every setter return type of the next setter.
        concreteBuilder.setSand(takenSand).setGranite(takenGranite).setCement(takenCement).setWater(takenWater);

        if(!concreteBuilder.checker()) { // if check not successful - clean all resources
            mSandMass = 0;
            mGraniteMass = 0;
            mCementMass = 0;
            mWaterMass = 0;
        }
    }

    public GraniteSetter setSand(final double pSandMass) {
        mSandMass = pSandMass;
        return new GraniteSetter();
    }

    public class GraniteSetter {
        public GraniteSetter() {}
        public CementSetter setGranite(final double pGraniteMass) {
            mGraniteMass = pGraniteMass;
            return new CementSetter();
        }

        public class CementSetter {
            public CementSetter() {}
            public WaterSetter setCement(final double pCementMass) {
                mCementMass = pCementMass;
                return new WaterSetter();
            }

            public class WaterSetter {
                public WaterSetter() {}
                public void setWater(final double pWaterMass){
                    mWaterMass = pWaterMass;
                }
            }
        }
    }

    /**
     * Check that we have resources at correct proportion.
     */
    private boolean checker() {
        mConcreteMass += mSandMass + mGraniteMass + mCementMass + mWaterMass;
        double onePercent = mConcreteMass / 100;
        if (mCementMass  != onePercent * 10) return false;
        if (mSandMass    != onePercent * 50) return false;
        if (mGraniteMass != onePercent * 25) return false;
        if (mWaterMass   != onePercent * 15) return false;
        return true;
    }

    public double getSandMass() {
        return mSandMass;
    }

    public double getGraniteMass() {
        return mGraniteMass;
    }

    public double getCementMass() {
        return mCementMass;
    }

    public double getWaterMass() {
        return mWaterMass;
    }

    public static int getConcreteMass() {
        return mConcreteMass;
    }

    public static void addSandMass(final int pMSandMass) {
        mSandMass += pMSandMass;
    }

    public static void addGraniteMass(final int pMGraniteMass) {
        mGraniteMass += pMGraniteMass;
    }

    public static void addCementMass(final int pMCementMass) {
        mCementMass += pMCementMass;
    }

    public static void addWaterMass(final int pMWaterMass) {
        mWaterMass += pMWaterMass;
    }

    public static void setmConcreteMass(final int pMConcreteMass) {
        mConcreteMass = pMConcreteMass;
    }

    public static void minusFromTotalConcrete(final int pMinus){
        mConcreteMass -= pMinus;
    }
}
