package auto;

/**
 * Special type of auto that delivery produced concrete.
 */
public class Mixer {

    private int mCapacity = 10;

    /**
     * mass
     */
    private int mConcrete;

    /**
     * Not used now - just for illustration.
     */
    private boolean go;

    public Mixer() { }

    public int getConcrete() {
        return mConcrete;
    }

    public void setConcrete(final int pConcrete) {
        mConcrete = pConcrete;
    }

    public int getCapacity() {
        return mCapacity;
    }

    public boolean isGo() {
        return go;
    }

    public void go() {
        go = true;
    }

    public void stop() {
        go = false;
    }
}
