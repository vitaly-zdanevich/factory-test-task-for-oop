package auto;

public class AutoResourceBig extends AutoResourceBase {

    private static int mCapacity = 10;

    public AutoResourceBig() {
        super();
    }

    @Override
    public int[] getCapacity() {
        return new int[]{mCapacity};
    }
}
