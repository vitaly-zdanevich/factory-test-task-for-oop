package auto;

import components.Resource;

import java.lang.reflect.Constructor;

public class AutoResourceBase {

    private int mCapacity = 5;
    private Resource[] mResource;

    /** Subclasses auto also can moveForward and moveBack. Just for illustrating of OOP */
    public void moveForward(){ }
    public void moveBack(){ }

    public AutoResourceBase(){ }

    public int[] getCapacity() {
        return new int[]{mCapacity};
    }

    public Resource[] getResource() {
        return mResource;
    }

    /** When we at the start of the world initialise autos with resources */
    public void setResource(final Resource[] pResource) {
        mResource = pResource;
    }

    /** Get me what you have - we need this for creating concrete */
    public void releaseResource() {
        for(Resource resource : getResource()) {
            resource.release();
            setResource(null);
        }
    }

    /** In our perfect world we have unlimited resources - sand, granite, cement, water */
    public void fillResourceDouble() {
        Resource[] resourcesTypes = getResource();
        Resource[] array = new Resource[resourcesTypes.length];
        int i = 0;
        while (i < resourcesTypes.length) {
            Resource resource = resourcesTypes[i];
            Class clas = resource.getClass();
            Constructor[] constructors = clas.getConstructors();
            int capacity = getCapacity()[i];
            try {
                array[i] = (Resource)constructors[0].newInstance(capacity);
            } catch (Exception e) {
                System.out.println("Problem with constructors[0].newInstance(capacity)");
                e.printStackTrace();
            }
            i++;
        }
        setResource(array);
    }

}
