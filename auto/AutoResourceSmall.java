package auto;

public class AutoResourceSmall extends AutoResourceBase {
    private int mCapacity = 5;

    public AutoResourceSmall() {
        super();
    }

    @Override
    public int[] getCapacity() {
        return new int[]{mCapacity};
    }
}
