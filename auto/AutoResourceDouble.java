package auto;

/**
 * ..."в некоторых случаях песок и гранит может быть доставлен одним и тем же автомобилем"
 */
public class AutoResourceDouble extends AutoResourceBase {
    private int mCapacitySand = 5;
    private int mCapacityGranite = 5;

    public AutoResourceDouble() {
        super();
    }

    /**
     * Double auto have not one capacity like other normal autos - but two capacity for each resource.
     * @return
     */
    @Override
    public int[] getCapacity() {
        return new int[]{mCapacitySand, mCapacityGranite};
    }
}
